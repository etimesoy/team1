import datetime

# классы: пользователь, музыкант, ученик, сообщество, плейлист
class User():
    def __init__(self, name, surname, age):
        self.name = name
        self.surname = surname
        self.__age = age
    
    def print_info(self):
        print("Name:", self.name)
        print("Surname:", self.surname)
        print("Age:", self.__age)
    
    @property
    def age(self):
        return self.__age
    
    @age.setter
    def age(self, age):
        if isinstance(age, (str, tuple)):
            print("Недопустимый возраст")
        elif age not in range(1, 150):
            print("Недопустимый возраст")
        else:
            self.__age = age


class Singer(User):
    def __init__(self, name, surname, age, songs_list, release_date):
        User.__init__(self, name, surname, age)
        self.songs_list = songs_list
        self.release_date = release_date
    
    # переопределение метода print_info
    def print_info(self):
        User.print_info(self)
        print("Count of songs:", len(self.songs_list))
        print("Songs: ", end="")
        print(*self.songs_list, sep=", ")
    
    def print_experience(self):
        print("Your first song was uploaded at: ", self.release_date)
        print(f"Since then, {len(self.songs_list)} songs have been uploaded")
    
    def add_song(self, song_name):
        self.songs_list.append(song_name)
    
    def print_songs(self):
        print("Songs: ", end="")
        print(*self.songs_list, sep=", ")


class Pupil(User):
    def __init__(self, name, surname, age, study_place, birth_month, birth_day):
        User.__init__(self, name, surname, age)
        self.study_place = study_place
        self.birth_month = birth_month
        self.birth_day = birth_day
    
    # переопределение метода print_info
    def print_info(self):
        User.print_info(self)
        print("Study place:", self.study_place)
        print("Birth month:", self.birth_month)
        print("Birth day:", self.birth_day)
    
    def check_birthday(self):
        now = datetime.datetime.now()
        if now.month == self.birth_month and now.day == self.day:
            self.age += 1
        return now.month == self.birth_month and now.day == self.day
    
    def get_marks_from_score(self):
        mark_dict = {}
        if len(self.score_dict) == 0:
            print("There are no subjects")
        for subject in self.score_dict:
            if self.score_dict[subject] < 56:
                mark_dict[subject] = 2
            elif self.score_dict[subject] < 72:
                mark_dict[subject] = 3
            elif self.score_dict[subject] < 86:
                mark_dict[subject] = 4
            else:
                mark_dict[subject] = 5
        return mark_dict


class Group():
    def __init__(self, name, description, creation_year, count_of_members):
        self.name = name
        self.description = description
        self.creation_year = creation_year
        self.count_of_members = count_of_members
    
    def congratulation_on_members_number(self):
        if self.count_of_members % 1000 == 0:
            return f"Congratulations! You have {self.count_of_members} participants!"
        else:
            return f"You have {self.count_of_members} participants"
    
    def check_anniversary(self):
        now = datetime.datetime.now()
        if (now.year - self.creation_year) % 10 == 0:
            return True, ("Congratulations on your anniversary!\n" +
                f"Your group is now {now.year - self.creation_year} years old!")
        else:
            return False, f"Your group is {now.year - self.creation_year} years old"

    def print_info(self):
        print("Name:", self.name)
        print("Description:", self.description)
        print("Creation_year:", self.creation_year)
        print("Count_of_members:", self.count_of_members)


class Playlist():
    def __init__(self, title_album, amount_of_music, genres_music, quantity_preferred):
        self.title_album = title_album
        self.amount_of_music = amount_of_music
        self.genres_music = genres_music
        self.quantity_preferred = quantity_preferred
    
    def type_of_music(self):
        print("Here you can find music in the genre: " + (', '.join(self.genres_music)))
    
    def favorite_album(self):
        if self.quantity_preferred >= (self.amount_of_music) // 2:
            self.title_album = self.title_album + " " + "(loved)"
        return self.quantity_preferred >= (self.amount_of_music) // 2
    
    def print_info(self):
        print("Title_album:", self.title_album)
        print("Genres music:", self.genres_music)
        print("Amount of music:", self.amount_of_music)
        print("Quantity_ preferred:", self.quantity_preferred)

print("----------------Group----------------")
group_1 = Group("Python",
    "Here you can find all information about python language", 2000, 50000)
group_1.print_info()
print(group_1.congratulation_on_members_number())
is_anniversary, message = group_1.check_anniversary()
if is_anniversary:
    print(message)

print()

print("----------------Playlist----------------")
playlist_1 = Playlist("Child", 10, "classical", 5)
playlist_1.print_info()
if playlist_1.favorite_album():
    print(f"The album status changed, {playlist_1.title_album}")

print()

print("----------------Singer----------------")
singer_1 = Singer("Petya", "Volkov", 22, ["first_song", "second_song"], "17.11.2019")
singer_1.print_info()
singer_1.print_experience()
singer_1.add_song("third_song")
singer_1.print_songs()

print()

print("----------------Pupil----------------")
pupil_1 = Pupil("Ivan", "Petrov", 18, "school", 6, 15)
pupil_1.print_info()
count_of_subjects = int(input("Please type in the number of subjects you have: "))
pupil_1.score_dict = {}
for i in range(count_of_subjects):
    message = f"Write the name of {i+1} subject and the number of points you have separated by a space: "
    subject, count = input(message).split()
    pupil_1.score_dict[subject] = int(count)
mark_dict = pupil_1.get_marks_from_score()
for subject in mark_dict:
    print(f"Subject: {subject}, mark: {mark_dict[subject]}")
if pupil_1.check_birthday():
    print(f"Happy birthday, {self.name}!")

print()

print("----------------User----------------")
user_1 = User("Kolya", "Bogdanov", 25)
user_1.print_info()
user_1.age = 30
print("Age:", user_1.age)
